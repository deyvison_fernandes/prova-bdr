# Instalação #
1. Executar script do banco de dados que está no arquivo “banco de dados.sql”
2. Copiar pasta “gerenciador-tarefas” para o servidor em que irá rodar a aplicação
3. Configurar conexão com o banco de dados no arquivo “gerenciador-tarefas\app\Config\database.php”

```
#!php

public $default = array(
    'datasource' => 'Database/Mysql',
    'persistent' => false,
    'host' => 'localhost',
    'login' => 'root',
    'password' => '',
    'database' => 'gerenciador-tarefas',
    'prefix' => '',
    //'encoding' => 'utf8',
);
```

# Requisições para a API #

* Para adicionar uma tarefa deve ser feito uma requisição do tipo POST para http://{host}/tarefas/add.json enviando os dados titulo, descricao e ordem no formato FORM ENCODED.

* Para editar uma tarefa deve ser feito uma requisição do tipo PUT para http://{host}/tarefas/edit/{id-tarefa}.json enviando os dados titulo, descricao e ordem no formato FORM ENCODED.

* Para deletar uma tarefa deve ser feito uma requisição do tipo DELETE para http://{host}/tarefas/delete/{id-tarefa}.json.

* Para listar as tarefas deve ser feito uma requisição do tipo GET para http://{host}/tarefas.json.

* Para visualizar os dados de uma tarefa deve ser feito uma requisição do tipo GET para http://{host}/tarefas/view/{id-tarefa}.json.

