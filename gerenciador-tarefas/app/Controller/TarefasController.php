<?php

class TarefasController extends AppController
{
    public $components = array('RequestHandler');

    public function index()
    {
        $tarefas = $this->Tarefa->find('all', array(
            'order' => array('ordem')
        ));
        $this->set(array(
            'tarefas' => $tarefas,
            '_serialize' => array('tarefas')
        ));
    }

    public function view($id)
    {
        $tarefa = $this->Tarefa->findById($id);
        $this->set(array(
            'tarefa' => $tarefa,
            '_serialize' => array('tarefa')
        ));
    }

    public function add()
    {
        $retorno = array(
            'sucesso' => false
        );

        $this->Tarefa->create();
        if ($this->Tarefa->save($this->request->data)) {
            $retorno['sucesso'] = true;
            $retorno['mensagem'] = 'Salvo';
        } else {
            $retorno['mensagem'] = 'Erro ao salvar';
        }
        $this->set(array(
            'retorno' => $retorno,
            '_serialize' => array('retorno')
        ));
    }

    public function edit($id)
    {
        $retorno = array(
            'sucesso' => false,
            'mensagem' => 'Não encontrado'
        );

        $this->Tarefa->id = $id;
        if($this->Tarefa->exists()) {
            if ($this->Tarefa->save($this->request->data)) {
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Salvo';
            } else {
                $retorno['mensagem'] = 'Erro ao salvar';
            }
        }
        $this->set(array(
            'retorno' => $retorno,
            '_serialize' => array('retorno')
        ));
    }

    public function delete($id)
    {
        $retorno = array(
            'sucesso' => false,
            'mensagem' => 'Não encontrado'
        );

        $this->Tarefa->id = $id;
        if($this->Tarefa->exists()) {
            if ($this->Tarefa->delete($id)) {
                $retorno['sucesso'] = true;
                $retorno['mensagem'] = 'Deletado';
            } else {
                $retorno['mensagem'] = 'Erro ao deletar';
            }
        }

        $this->set(array(
            'retorno' => $retorno,
            '_serialize' => array('retorno')
        ));
    }
}

