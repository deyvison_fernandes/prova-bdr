<?php
for ($cont = 1; $cont <= 100; $cont++) {
    $resultado = '';

    if (($cont % 3) == 0) {
        $resultado = 'Fizz';
    }

    if (($cont % 5) == 0) {
        $resultado .= 'Buzz';
    }

    $resultado = ($resultado == '') ? $cont : $resultado;

    echo $resultado.'<br />';
}
